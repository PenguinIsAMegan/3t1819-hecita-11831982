#include <iostream>
#include <string>
#include <stdlib.h>
#include <vector>
#include "Pokemon.h"
#include "Player.h"

using namespace std;

void createPlayer(Player* playerInfo, Pokemon* pokemonInfo)
{
	playerInfo->getName();
	cout << endl;
	playerInfo->startingPokemon(pokemonInfo->WildPokemonStat);
	cout << endl;
	playerInfo->collectPokemon(playerInfo->pokemonStat);
	cout << endl << endl;
	playerInfo->displayPlayerCollection(playerInfo->pokemonStat);
	cout << endl << endl;
}

void playerChoice(Player* playerInfo, int& pChoice)
{
	cout << "What would you like to do, Trainer " << playerInfo->name << "?" << endl;
	cout << "[1] - Move" << endl;
	cout << "[2] - Pokemons" << endl;

	if (playerInfo->isSafeLocation == true)
	{
		cout << "[3] - Pokemon Center" << endl;
	}
	cin >> pChoice;
}

void battlePokemon(Player* playerInfo, Pokemon* pokemonInfo, int& ctr, int& index)
{
	int chance = rand() % 100 + 1;
	int eChance = rand() % 100 + 1;

	if (ctr > 0)
	{
		if (pokemonInfo->WildPokemonStat->hp > 0 && playerInfo->playersCollection[index]->hp > 0)
		{
			if (chance <= 80)
			{
				cout << playerInfo->playersCollection[index]->name << " attacked " << pokemonInfo->WildPokemonStat->name << "!" << endl;
				pokemonInfo->WildPokemonStat->hp -= playerInfo->playersCollection[index]->baseDamage;
				cout << pokemonInfo->WildPokemonStat->name << " took " << playerInfo->playersCollection[index]->baseDamage << " damage!" << endl;
				cout << pokemonInfo->WildPokemonStat->name << " has " << pokemonInfo->WildPokemonStat->hp << " left!" << endl << endl;
			}

			else
			{
				cout << playerInfo->playersCollection[index]->name << " tried to attack, but missed!" << endl << endl;
			}

			if (eChance <= 80 && pokemonInfo->WildPokemonStat->hp > 0)
			{
				cout << pokemonInfo->WildPokemonStat->name << " attacked " << playerInfo->playersCollection[index]->name << "!" << endl;
				playerInfo->playersCollection[index]->hp -= pokemonInfo->WildPokemonStat->baseDamage;
				cout << playerInfo->playersCollection[index]->name << " took " << pokemonInfo->WildPokemonStat->baseDamage << " damage!" << endl;
				cout << playerInfo->playersCollection[index]->name << " has " << playerInfo->playersCollection[index]->hp << " left!" << endl << endl;
			}

			else if (eChance > 80 && pokemonInfo->WildPokemonStat->hp > 0)
			{
				cout << pokemonInfo->WildPokemonStat->name << " tried to attack, but missed!" << endl << endl;
			}
		}

		if (pokemonInfo->WildPokemonStat->hp <= 0)
		{
			cout << playerInfo->playersCollection[index]->name << " won!" << endl;
			cout << playerInfo->playersCollection[index]->name << " earned 25 exp!" << endl;
			playerInfo->playersCollection[index]->exp += 25;

			if (playerInfo->playersCollection[index]->exp >= playerInfo->playersCollection[index]->expToNextLevel)
			{
				cout << playerInfo->playersCollection[index]->name << " leveled up!" << endl << endl;
				playerInfo->levelPokemon(playerInfo->playersCollection[index]);
				playerInfo->playersCollection[index]->level += 1;

				cout << playerInfo->playersCollection[index]->name << "'s stats:" << endl;
				cout << "Level: " << playerInfo->playersCollection[index]->level << endl;
				cout << "Health Points: " << playerInfo->playersCollection[index]->hp << "/" << playerInfo->playersCollection[index]->baseHp << endl;
				cout << "Damage: " << playerInfo->playersCollection[index]->baseDamage << endl;
				cout << "Exp: " << playerInfo->playersCollection[index]->exp << "/" << playerInfo->playersCollection[index]->expToNextLevel << endl;
			}
		}

		if (playerInfo->playersCollection[index]->hp <= 0)
		{
			playerInfo->playersCollection[index]->hp = 0;

			for (int i = 0; i < playerInfo->playersCollection.size(); i++)
			{
				if (playerInfo->playersCollection[i]->hp > 0)
				{
					cout << playerInfo->playersCollection[index]->name << " fainted! Bringing out next Pokemon, ";
					index = i;
					cout << playerInfo->playersCollection[index]->name << "!" << endl << endl;
					ctr--;
					break;
				}
			}

			if (playerInfo->playersCollection[index]->hp <= 0)
			{
				cout << "All your Pokemons fainted!" << endl << endl;
				ctr = 0;
			}
		}
	}

	if (ctr <= 0)
	{
		cout << endl << endl << "No pokemon available for battle!" << endl << endl;
	}
}

void catchPokemon(Player* playerInfo, Pokemon* pokemonInfo, bool& isCatch, int& ctr)
{
	int chance = rand() % 100 + 1;

	if (chance <= 30)
	{
		playerInfo->collectPokemon(pokemonInfo->WildPokemonStat);
		ctr = playerInfo->playersCollection.size();
		isCatch = true;
	}

	else
	{
		cout << pokemonInfo->WildPokemonStat->name << " got out of the ball!" << endl << endl;
	}
}

void encounterPokemon(Player* playerInfo, Pokemon* pokemonInfo, bool& isCatch, int& ctr, int& index)
{
	int chance = rand() % 100 + 1;
	int choice;

	if (chance <= 50)
	{
		pokemonInfo->getPokemonList(playerInfo->location);
		pokemonInfo->displayPokemon(pokemonInfo->WildPokemonStat);

		while (pokemonInfo->WildPokemonStat->hp > 0 && isCatch == false)
		{
			cout << "What would you like to do?" << endl;

			cout << "[1] - Battle" << endl;
			cout << "[2] - Catch" << endl;
			cout << "[3] - Run Away" << endl;
			cin >> choice;

			if (choice == 1 && ctr <= 0)
			{
				cout << endl << endl << "No Pokemon available for battle!" << endl << endl;
			}

			else if (choice == 1)
			{
				battlePokemon(playerInfo, pokemonInfo, ctr, index);
			}

			else if (choice == 2)
			{
				catchPokemon(playerInfo, pokemonInfo, isCatch, ctr);
			}

			if (choice == 3)
			{
				cout << "You ran away!" << endl;
				break;
			}
		}
	}

	cout << endl << endl << "You continue walking..." << endl << endl;
}

void simulation(Player* playerInfo, Pokemon* pokemonInfo, int& pChoice, bool& isCatch, int& ctr, int& index)
{
	while (true)
	{
		isCatch = false;

		playerChoice(playerInfo, pChoice);

		switch (pChoice)
		{
		case 1: playerInfo->getLocation(playerInfo->x, playerInfo->y); break;
		case 2: playerInfo->displayPlayerCollection(playerInfo->pokemonStat); break;
		}

		if (playerInfo->isSafeLocation == true)
		{
			if (pChoice == 3)
			{
				playerInfo->healPokemon(playerInfo->pokemonStat);
				ctr = playerInfo->playersCollection.size();
				break;
			}
		}

		else
		{
			if (pChoice == 3)
			{
				cout << "You're not in a safe zone!" << endl;
			}

			cout << endl << endl;
			encounterPokemon(playerInfo, pokemonInfo, isCatch, ctr, index);
			cout << endl << endl;
		}

		system("pause");
		system("cls");
	}
}

int main()
{
	Player* playerInfo = new Player;
	Pokemon* pokemonInfo = new Pokemon;
	int index = 0;
	int pChoice;
	bool isCatch = false;

	createPlayer(playerInfo, pokemonInfo);
	int ctr = playerInfo->playersCollection.size();

	while (true)
	{
		simulation(playerInfo, pokemonInfo, pChoice, isCatch, ctr, index);
	}

	system("pause");

	delete playerInfo;
	delete pokemonInfo;

	return 0;
}