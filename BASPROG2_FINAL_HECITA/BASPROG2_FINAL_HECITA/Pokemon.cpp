#include "Pokemon.h"
#include <time.h>

Pokemon::Pokemon()
{
	this->name = "";
	this->baseHp = 0;
	this->hp = 0;
	this->level = 0;
	this->baseDamage = 0;
	this->exp = 0;
	this->expToNextLevel = 0;
}

Pokemon::Pokemon(string name, int baseHp, int hp, int level, int baseDamage, int exp, int expToNextLevel)
{
	this->name = name;
	this->baseHp = baseHp;
	this->hp = hp;
	this->level = level;
	this->baseDamage = baseDamage;
	this->exp = exp;
	this->expToNextLevel = expToNextLevel;
}

void Pokemon::getPokemonList(string location)
{
	srand(time(NULL));
	int randLevel = rand() % 10 + 1;

	if (location == "Route 1")
	{
		int pokemon = rand() % 4 + 1;

		switch (pokemon)
		{
		case 1: this->WildPokemonStat = new Pokemon("Pidgey", 40, 40, randLevel, 45, 0, 100); break;
		case 2: this->WildPokemonStat = new Pokemon("Rattata", 30, 30, randLevel, 56, 0, 100); break;
		case 3: this->WildPokemonStat = new Pokemon("Sentret", 35, 35, randLevel, 46, 0, 100); break;
		case 4: this->WildPokemonStat = new Pokemon("Furret", 85, 85, randLevel, 76, 0, 100); break;
		}
	}

	else if (location == "Route 2")
	{
		int pokemon = rand() % 6 + 1;

		switch (pokemon)
		{
		case 1: this->WildPokemonStat = new Pokemon("Pidgey", 40, 40, randLevel, 45, 0, 100); break;
		case 2: this->WildPokemonStat = new Pokemon("Caterpie", 45, 45, randLevel, 30, 0, 100); break;
		case 3: this->WildPokemonStat = new Pokemon("Butterfree", 60, 60, randLevel, 45, 0, 100); break;
		case 4: this->WildPokemonStat = new Pokemon("Pikachu", 35, 35, randLevel, 55, 0, 100); break;
		case 5: this->WildPokemonStat = new Pokemon("Ledyba", 40, 40, randLevel, 20, 0, 100); break;
		case 6: this->WildPokemonStat = new Pokemon("Ledian", 55, 55, randLevel, 35, 0, 100); break;
		}
	}

	else if (location == "Route 3")
	{
		int pokemon = rand() % 6 + 1;

		switch (pokemon)
		{
		case 1: this->WildPokemonStat = new Pokemon("Rattata", 30, 30, randLevel, 56, 0, 100); break;
		case 2: this->WildPokemonStat = new Pokemon("Raticate", 55, 55, randLevel, 81, 0, 100); break;
		case 3: this->WildPokemonStat = new Pokemon("Spearow", 40, 40, randLevel, 60, 0, 100); break;
		case 4: this->WildPokemonStat = new Pokemon("Ekans", 35, 35, randLevel, 60, 0, 100); break;
		case 5: this->WildPokemonStat = new Pokemon("Arbok", 60, 60, randLevel, 85, 0, 100); break;
		case 6: this->WildPokemonStat = new Pokemon("Sandshrew", 50, 50, randLevel, 75, 0, 100); break;
		}
	}

	else if (location == "Mt. Moon")
	{
		int pokemon = rand() % 5 + 1;

		switch (pokemon)
		{
		case 1: this->WildPokemonStat = new Pokemon("Sandshrew", 50, 50, randLevel, 75, 0, 100); break;
		case 2: this->WildPokemonStat = new Pokemon("Clefairy", 70, 70, randLevel, 45, 0, 100); break;
		case 3: this->WildPokemonStat = new Pokemon("Zubat", 40, 40, randLevel, 45, 0, 100); break;
		case 4: this->WildPokemonStat = new Pokemon("Paras", 35, 35, randLevel, 70, 0, 100); break;
		case 5: this->WildPokemonStat = new Pokemon("Geodude", 40, 40, randLevel, 80, 0, 100); break;
		}
	}

	else if (location == "Route 4")
	{
		int pokemon = rand() % 6 + 1;

		switch (pokemon)
		{
		case 1: this->WildPokemonStat = new Pokemon("Rattata", 30, 30, randLevel, 56, 0, 100); break;
		case 2: this->WildPokemonStat = new Pokemon("Raticate", 55, 55, randLevel, 81, 0, 100); break;
		case 3: this->WildPokemonStat = new Pokemon("Spearow", 40, 40, randLevel, 60, 0, 100); break;
		case 4: this->WildPokemonStat = new Pokemon("Ekans", 35, 35, randLevel, 60, 0, 100); break;
		case 5: this->WildPokemonStat = new Pokemon("Arbok", 60, 60, randLevel, 85, 0, 100); break;
		case 6: this->WildPokemonStat = new Pokemon("Sandshrew", 50, 50, randLevel, 75, 0, 100); break;
		}
	}

	else if (location == "Route 24")
	{
		int pokemon = rand() % 3 + 1;

		switch (pokemon)
		{
		case 1: this->WildPokemonStat = new Pokemon("Abra", 25, 25, randLevel, 20, 0, 100); break;
		case 2: this->WildPokemonStat = new Pokemon("Bellsprout", 50, 50, randLevel, 75, 0, 100); break;
		case 3: this->WildPokemonStat = new Pokemon("Sunkern", 30, 30, randLevel, 30, 0, 100); break;
		}
	}
}

void Pokemon::levelPokemon(Pokemon * WildPokemonStat)
{
	this->WildPokemonStat->baseHp += (WildPokemonStat->baseHp * 0.15);
	this->WildPokemonStat->hp = WildPokemonStat->baseHp;
	this->WildPokemonStat->baseDamage += (WildPokemonStat->baseDamage * 0.10);
	this->WildPokemonStat->exp = 0;
	this->WildPokemonStat->expToNextLevel += (WildPokemonStat->expToNextLevel * 0.20);
}

void Pokemon::displayPokemon(Pokemon* WildPokemonStat)
{
	if (this->WildPokemonStat->level > 1)
	{
		for (int i = 1; i < WildPokemonStat->level; i++)
		{
			this->levelPokemon(WildPokemonStat);
		}
	}

	cout << "You have encountered a wild " << this->WildPokemonStat->name << "!" << endl << endl;
	cout << this->WildPokemonStat->name << "'s stats:" << endl;
	cout << "Level: " << this->WildPokemonStat->level << endl;
	cout << "Health Points: " << this->WildPokemonStat->hp << "/" << this->WildPokemonStat->baseHp << endl;
	cout << "Damage: " << this->WildPokemonStat->baseDamage << endl;
	cout << "Exp: " << this->WildPokemonStat->exp << "/" << this->WildPokemonStat->expToNextLevel << endl;
}