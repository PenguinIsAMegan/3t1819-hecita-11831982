#pragma once

#include <string>
#include "Pokemon.h"
#include <vector>
#include <iostream>

using namespace std;

class Player
{
public:
	Player();
	Player(string name);
	string name;
	void getName();
	int x = 0;
	int y = 0;
	string location;
	void getLocation(int& x, int& y);
	bool isSafeLocation = true;
	vector<Pokemon*> playersCollection;
	Pokemon* pokemonStat;
	void startingPokemon(Pokemon* pokemonStat);
	void collectPokemon(Pokemon* pokemonStat);
	void levelPokemon(Pokemon* pokemonStat);
	void displayPlayerCollection(Pokemon * pokemonStat);
	void healPokemon(Pokemon* pokemonStat);
};