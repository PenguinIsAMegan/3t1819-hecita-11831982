#pragma once

#include <string>
#include <iostream>

using namespace std;

class Pokemon
{
public:
	Pokemon();
	Pokemon(string name, int baseHp, int hp, int level, int baseDamage, int exp, int expToNextLevel);
	string name;
	int baseHp;
	int hp;
	int level;
	int baseDamage;
	int exp;
	int expToNextLevel;
	Pokemon* WildPokemonStat;
	void getPokemonList(string location);
	string location;
	void levelPokemon(Pokemon* WildPokemonStat);
	void displayPokemon(Pokemon* WildPokemonStat);
};