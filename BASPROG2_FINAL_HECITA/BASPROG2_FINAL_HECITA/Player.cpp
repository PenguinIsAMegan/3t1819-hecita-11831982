#include "Player.h"
#include <iostream>
#include <string>
#include <time.h>

using namespace std;

Player::Player()
{
	this->name = "";
}

Player::Player(string name)
{
	this->name = name;
}

void Player::getName()
{
	cout << "Hi, trainer! What's your name?: ";
	cin >> this->name;
}

void Player::getLocation(int& x, int& y)
{
	char choice;

	cout << "Where will you go?" << endl;
	cout << "[W] - Up" << endl;
	cout << "[A] - Left" << endl;
	cout << "[S] - Down" << endl;
	cout << "[D] - Right" << endl;
	cin >> choice;

	if (choice == 'w' || choice == 'W')
	{
		y++;
	}

	else if (choice == 'a' || choice == 'A')
	{
		x--;
	}

	else if (choice == 's' || choice == 'S')
	{
		y--;
	}

	else if (choice == 'd' || choice == 'D')
	{
		x++;
	}

	if ((x >= -2 && x <= 2) && (y >= -2 && y <= 2))
	{
		this->location = "Pallet Town";
		this->isSafeLocation = true;
	}

	else if ((x >= -2 && x <= 2) && (y >= 3 && y <= 7))
	{
		this->location = "Route 1";
		this->isSafeLocation = false;
	}

	else if ((x >= -2 && x <= 2) && (y >= 8 && y <= 12))
	{
		this->location = "Virdian City";
		this->isSafeLocation = true;
	}

	else if ((x >= -2 && x <= 2) && (y >= 13 && y <= 17))
	{
		this->location = "Route 2";
		this->isSafeLocation = false;
	}

	else if ((x >= -2 && x <= 2) && (y >= 18 && y <= 22))
	{
		this->location = "Pewter City";
		this->isSafeLocation = true;
	}

	else if ((x >= 3 && x <= 7) && (y >= 18 && y <= 22))
	{
		this->location = "Route 3";
		this->isSafeLocation = false;
	}

	else if ((x >= 3 && x <= 7) && (y >= 23 && y <= 27))
	{
		this->location = "Mt. Moon";
		this->isSafeLocation = false;
	}

	else if ((x >= 8 && x <= 12) && (y >= 23 && y <= 27))
	{
		this->location = "Route 4";
		this->isSafeLocation = false;
	}

	else if ((x >= 13 && x <= 17) && (y >= 23 && y <= 27))
	{
		this->location = "Cerulean City";
		this->isSafeLocation = true;
	}

	else if ((x >= 13 && x <= 17) && (y >= 28 && 32))
	{
		this->location = "Route 24";
		this->isSafeLocation = false;
	}

	else
	{
		this->location = "Unknown Location";
		this->isSafeLocation = true;
	}

	cout << "You're location is now (" << x << "," << y << ")" << endl;
	cout << "You're current location is " << this->location << endl;
}

void Player::startingPokemon(Pokemon * pokemonStats)
{
	int sChoice;
	cout << "Trainer " << this->name << ", choose your starting pokemon!" << endl << endl;
	cout << "[1] - Bulbasaur" << endl;
	cout << "[2] - Charmander" << endl;
	cout << "[3] - Squirtle" << endl << endl;

	cin >> sChoice;

	switch (sChoice)
	{
	case 1: this->pokemonStat = new Pokemon("Bulbasaur", 45, 45, 5, 49, 0, 100); break;
	case 2: this->pokemonStat = new Pokemon("Charmander", 39, 39, 5, 52, 0, 100); break;
	case 3: this->pokemonStat = new Pokemon("Squirtle", 44, 44, 5, 48, 0, 100); break;
	}

	for (int i = 1; i < 5; i++)
	{
		this->levelPokemon(pokemonStat);
	}
}

void Player::collectPokemon(Pokemon * pokemonStat)
{
	cout << "You got " << pokemonStat->name << "!" << endl;
	this->playersCollection.push_back(pokemonStat);
}

void Player::levelPokemon(Pokemon * pokemonStat)
{
	this->pokemonStat->baseHp += (pokemonStat->baseHp * 0.15);
	this->pokemonStat->hp = pokemonStat->baseHp;
	this->pokemonStat->baseDamage += (pokemonStat->baseDamage * 0.10);
	this->pokemonStat->exp = 0;
	this->pokemonStat->expToNextLevel += (pokemonStat->expToNextLevel * 0.20);
}

void Player::displayPlayerCollection(Pokemon * pokemonStat)
{
	for (int i = 0; i < this->playersCollection.size(); i++)
	{
		cout << this->playersCollection[i]->name << "'s stats:" << endl;
		cout << "Level: " << this->playersCollection[i]->level << endl;
		cout << "Health Points: " << this->playersCollection[i]->hp << "/" << this->playersCollection[i]->baseHp << endl;
		cout << "Damage: " << this->playersCollection[i]->baseDamage << endl;
		cout << "Exp: " << this->playersCollection[i]->exp << "/" << this->playersCollection[i]->expToNextLevel << endl << endl << endl;
	}

	system("pause");
	system("cls");
}

void Player::healPokemon(Pokemon * pokemonStat)
{
	for (int i = 0; i < this->playersCollection.size(); i++)
	{
		this->playersCollection[i]->hp = this->playersCollection[i]->baseHp;
	}

	cout << "All Pokemons healed!" << endl << endl;
}